<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>PhoneBook</title>
	<link rel="stylesheet" type="text/css" href="{{asset('public/css/app.css')}}">
</head>
<body>
	<div id="app">
		<myheader></myheader>
		<div class="container">
			<router-view></router-view>
		</div>
		<myfooter></myfooter>
	</div>

	<script type="text/javascript" src="{{asset('public/js/app.js')}}"></script>
</body>
</html>