require('./bootstrap');

window.Vue = require('vue');

import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)


var flash    = Vue.component('flash', require('./components/Flash.vue').default);
var home     = Vue.component('home', require('./components/Home.vue').default);
var about    = Vue.component('about', require('./components/About.vue').default);
var myheader = Vue.component('myheader', require('./components/Myheader.vue').default);
var myfooter = Vue.component('myfooter', require('./components/Myfooter.vue').default);

const routes = [
  { path: '/home', component: home },
  { path: '/about', component: about }
]

const router = new VueRouter({
	// mode:'history',
  routes // short for `routes: routes`
})

const app = new Vue({
    el: '#app',
    router,
    components:{myheader,myheader}
});